function triangulo1(a, b, c) {
    if (a != 0 && b != 0 && c != 0) {
        if (Math.abs(a - b) < c && c < (a+b)) {
            if (Math.abs(a - c) < b && b  < (a+c)) {
                if( Math.abs(b - c) < a && a < (b+c)) {
                    console.log("É um triângulo");
                } else console.log("Não é um triângulo!");
            } else console.log("Não é um triângulo!");
        } else console.log("Não é um triângulo!");
    } else console.log("Não pode haver valor nulo!");
}
  
triangulo1(1, 2, 3);
triangulo1(1, 2, 2);
triangulo1(1, 0, 3);

function triangulo1(a, b, c) {
    if (a != 0 && b != 0 && c != 0) {
        if (teste(a, b, c) &&
        teste(a, c, b) &&
        teste(b, c, a)) console.log("É um triângulo!");
        else console.log("Não é um triângulo!");
        }else console.log("Não pode haver valor nulo!");
        }
        
  
triangulo1(1, 2, 3);
triangulo1(1, 2, 2);
triangulo1(1, 0, 3);